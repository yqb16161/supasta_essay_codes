#!/usr/bin/env python
"""Get the absorption and dispersion spectra for a two level atom."""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

# define parameters
W21 = 0.2
gamma = 1
rab = np.linspace(0.1, 1, 5)
delta = np.linspace(-10, 10, 1000)

store = np.zeros((len(delta), len(rab), 2))
legends = []

for i in range(len(rab)):

    g = rab[i]
    legends.append((r'$g=%0.2f$' % g))

    R12 = W21 * (g * delta + np.complex(0, 1) * g * gamma)
    R12 /= (((gamma ** 2) + (delta ** 2)) * W21) + 4 * g * g * gamma

    store[:, i, 0] = np.imag(R12)
    store[:, i, 1] = np.real(R12)

plt.figure(figsize=(6, 3))
plt.plot(delta, store[:, :, 0])
plt.xlim(min(delta), max(delta))
plt.grid()
plt.legend(legends, fontsize=15)
plt.xlabel(r'$\Delta$ (arb units)', fontsize=15)
plt.ylabel(r'Intensity (arb units)', fontsize=15)
plt.title(r'Absorption Spectra', fontsize=15)
plt.tight_layout(pad=0.1)
plt.savefig('absorption.png', format='png', dpi=300)

plt.figure(figsize=(6, 3))
plt.plot(delta, store[:, :, 1])
plt.xlim(min(delta), max(delta))
plt.grid()
plt.legend(legends, fontsize=15)
plt.xlabel(r'$\Delta$ (arb units)', fontsize=15)
plt.ylabel(r'Intensity (arb units)', fontsize=15)
plt.title(r'Dispersion', fontsize=15)
plt.tight_layout(pad=0.1)
plt.savefig('dispersion.png', format='png', dpi=300)

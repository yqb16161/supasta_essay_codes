#!/usr/bin/env python
"""Python script used for supasta essay purposes."""

import matplotlib.pyplot as plt
from matplotlib import rc
import numpy as np

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

def get_intensity(gamma, omega, delta_v, laser_freq):
    """Return the mollow specra for given input configuration."""
    freq = np.linspace(- 100 * 10 ** 6, 100 * 10 ** 6, 1000)

    n0 = np.divide((0.25 * omega ** 2),
                   ((0.5 * omega ** 2) + (delta_v ** 2) + (0.25 * gamma ** 2)))

    intensity1 = 8. * np.pi * (n0 ** 2) * (0.25 * (gamma ** 2) + (delta_v ** 2))
    intensity1 /= omega ** 2

    intensity2 = gamma * n0 * omega ** 2
    intensity2 *= (freq - laser_freq) ** 2 + 0.5 * omega ** 2 + gamma ** 2
    intensity2b = gamma ** 2 * ((omega ** 2 / (4 * n0)) - 2 *
                                (freq - laser_freq) ** 2) ** 2

    intensity2b += (freq - laser_freq) ** 2 * (omega ** 2 + delta_v ** 2 +
                                               (5./4.) * gamma ** 2 -
                                               (freq - laser_freq)
                                               ** 2) ** 2
    intensity2 /= intensity2b

    intensity = intensity2

    return(intensity)


gamma = 5 * 10 ** 6                # hertz
omega = 30 * 10 ** 6                # hertz
delta_v = 0
laser_freq = 5890 * 10 ** -8       # hertz


freq = np.linspace(- 100 * 10 ** 6, 100 * 10 ** 6, 1000)

varying_parameter = np.array([0, 5, 10, 15, 20, 25]) * 10 ** 6

intensity = np.zeros((len(freq), len(varying_parameter)))

legends = []

for i in range(0, len(varying_parameter)):

    delta_v = varying_parameter[i]

    intensity[:, i] = get_intensity(gamma, omega, delta_v, laser_freq)

    legends.append((r'$\Delta\nu = %0.0f$ $MHz$' % (delta_v / 10 ** 6)))


plt.figure(figsize=(6, 3))
plt.plot(freq, intensity)
plt.xlabel(r'Emission frequency ($Hz$)', fontsize=15)
plt.ylabel(r'Emission intensity (arb units)', fontsize=15)
plt.title(r'Mollow Spectra', fontsize=15)
plt.legend(legends, fontsize=10)
plt.tight_layout(pad=0.2)
plt.grid()
plt.savefig('varying_delta.png', format='png', dpi=300)

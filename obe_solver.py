#!/usr/bin/env python
"""Numerical solver for the optical bloch equations."""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from scipy.integrate import ode

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)


def f(t, y, par):
    """Function to store the differential equations."""
    g, d, W21, W12, gamma = par[0], par[1], par[2], par[3], par[4]
    return([1j * g * (y[1] - y[2]) + W21 * y[3] - W12 * y[0],
            1j * g * (y[0] - y[3]) - (gamma + 1j * d) * y[1],
            1j * g * (y[3] - y[0]) - (gamma - 1j * d) * y[2],
            1j * g * (y[2] - y[1]) + W12 * y[0] - W21 * y[3]])


def jac(t, y, par):
    """Function to store the jacobian."""
    g, d, W21, W12, gamma = par[0], par[1], par[2], par[3], par[4]
    return([[-W12, 1j * g, - 1j * g, W21],
            [1j * g, - (gamma + 1j * d), 0, - 1j * g],
            [- 1j * g, 0, (1j * d - gamma), 1j * g],
            [W12, -1j * g, 1j * g, -W21]])


g = 1
d = 1
W21 = 0.2
W12 = 0
gamma = 0.5

par = [g, d, W21, W12, gamma]

y0, t0 = [1, 0, 0, 0], 0

r = ode(f, jac).set_integrator('zvode', method='bdf')
r.set_initial_value(y0, t0).set_f_params(par).set_jac_params(par)
t1 = 20
dt = .01

times = []

r11 = []
r12 = []
r21 = []
r22 = []
while r.successful() and r.t < t1:
    a = r.integrate(r.t+dt)
    r11.append(a[0])
    r12.append(a[1])
    r21.append(a[2])
    r22.append(a[3])
    times.append(r.t)

plt.figure(figsize=(4, 2))
plt.subplot(121)
plt.plot(times, np.real(r11))
plt.plot(times, np.real(r22))
plt.xlim(0, t1)
plt.grid()
plt.legend([r'$R_{11}$', r'$R_{22}$'], fontsize=8)
plt.xlabel(r'Time ($s$)', fontsize=8)
plt.ylabel(r'Occupation Probability', fontsize=8)
plt.title(r'Progression to steady state', fontsize=8)

plt.subplot(122)
plt.plot(times, np.real(r12))
plt.plot(times, np.imag(r12))
plt.xlim(0, t1)
plt.grid()
plt.xlabel(r'Time ($s$)', fontsize=8)
plt.ylabel(r'$R_{12}$', fontsize=8)
plt.title(r'Progression to steady state', fontsize=8)
plt.legend([r'$\mathcal{R}e[R_{12}]$',
            r'$\mathcal{I}m[R_{12}]$'], fontsize=8)

plt.tight_layout(pad=0.2)

plt.savefig('steady_state.png', format='png', dpi=300)

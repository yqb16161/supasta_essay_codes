#!/usr/bin/env python
"""Numerical solver for the optical block equations."""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from scipy.integrate import ode

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)


def f(t, y, par):
    """Function to store the differential equations."""
    g, d = par[0], par[1]
    return([1j * g * y[1] - 1j * g * y[2],
            1j * g * y[0] - 1j * d * y[1] - 1j * g * y[3],
            -1j * g * y[0] + 1j * d * y[2] + 1j * g * y[3],
            -1j * g * y[1] + 1j * g * y[2]])


def jac(t, y, par):
    """Function to store the jacobian."""
    g, d = par[0], par[1]
    return([[0, 1j * g, - 1j * g, 0],
            [1j * g, - 1j * d, 0, - 1j * g],
            [- 1j * g, 0, 1j * d, 1j * g],
            [0, -1j * g, 1j * g, 0]])


g = 4
d = 0

par = [g, d]

y0, t0 = [1, 0, 0, 0], 0

r = ode(f, jac).set_integrator('zvode', method='bdf')
r.set_initial_value(y0, t0).set_f_params(par).set_jac_params(par)
t1 = 10
dt = .01

times = []

r11 = []
r22 = []
while r.successful() and r.t < t1:
    a = r.integrate(r.t+dt)
    r11.append(np.real(a[0]))
    r22.append(np.real(a[3]))
    times.append(r.t)

plot_title = 'population_evolution_g=' + str(g) + '.png'
plt.figure(figsize=(3, 1.5))
plt.plot(times, r11, times, r22, '--')
plt.xlabel(r'Time $(s)$', fontsize=10)
plt.ylabel(r'Population', fontsize=10)
plt.title((r'Population evolution for $g$ = %0.2f' % g), fontsize=10)
plt.legend([r'Ground state population',
            r'Excired State state population'],
           fontsize=6, loc='lower right')
plt.xticks(size=6)
plt.yticks(size=6)
plt.grid()
plt.tight_layout(pad=0.2)
plt.savefig(plot_title, format='png', dpi=500)
